const express = require('express') // Import express
const router = express.Router() // Make router from app
const PelangganController = require('../controllers/pelangganController.js') // Import PelangganController
const pelangganValidator = require('../middlewares/validators/pelangganValidator.js') // Import validator to validate every request from user

router.get('/', PelangganController.getAll) // If accessing localhost:3000/pelanggan, it will call getAll function in PelangganController class
router.get('/:id', PelangganController.getOne) // If accessing localhost:3000/pelanggan/:id, it will call getOne function in PelangganController class
router.post('/create', pelangganValidator.create, PelangganController.create) // If accessing localhost:3000/pelanggan/create, it will call create function in PelangganController class
router.put('/update/:id', pelangganValidator.update, PelangganController.update) // If accessing localhost:3000/pelanggan/update/:id, it will call update function in PelangganController class
router.delete('/delete/:id', PelangganController.delete) // If accessing localhost:3000/pelanggan/delete/:id, it will call delete function in PelangganController class

module.exports = router; // Export router
