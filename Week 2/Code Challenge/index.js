const data = require('./arrayFactory.js');
const test = require('./test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

console.log("Data asli tanpa null = ", clean(data));

// Should return array
function sortAscending(data) {
  // Code Here
  let length=Array.from(data).length
  for (i=0;i<length;i++) {
    for (j=0;j<length-i-1;j++) {
      if (data[j]>data[j+1]) {
        let dataDummy=data[j]
        data[j]=data[j+1]
        data[j+1]=dataDummy
      }
    }
  }
  return Array.from(clean(data));
}
let dataAsc=[]
dataAsc=sortAscending(data);
console.log("\nData terurut ascending = ", Array.from(dataAsc));
// Should return array
function sortDecending(data) {
  // Code Here
  let length=data.length
  for (i=0;i<length;i++) {
    for (j=0;j<length-i-1;j++) {
      if (data[j]<data[j+1]) {
        let dataDummy=data[j]
        data[j]=data[j+1]
        data[j+1]=dataDummy
      }
    }
  }
  return Array.from(clean(data));
}
let dataDesc=[]
dataDesc=sortDecending(data);
console.log("\nData terurut descending = ", Array.from(dataDesc));
// DON'T CHANGE
test(sortAscending, sortDecending, data);
