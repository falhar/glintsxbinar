//Code to calculate volume
const readline=require('readline');
const rl=readline.createInterface({
    input:process.stdin,
    output:process.stdout
})
function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}
function cube(s) {
    rl.question("Cube length : ", s=>{
        if (!isNaN(s)&&!isEmptyOrSpaces(s)) {
            console.log(`Volume = ${s**3}`);
            testAgain();
        } else {
            console.log("Input must be a number");
            cube();
        }
    })
}

function cone(r,h) {
    rl.question("radius = ", r=>{
        if (!isNaN(r)&&!isEmptyOrSpaces(r)) {
            function cone2() {rl.question("hight = ", h=>{
                if (!isNaN(h)&&!isEmptyOrSpaces(h)) {
                    console.log(`Volume = ${Math.PI*r*r*h/3}`);
                    testAgain();
                } else {
                    console.log("Input must be a number");
                    cone2();
                }
            })
        }
        cone2();
        } else {
            console.log("Input must be a number");
            cone();
        }
    })
}

function option(choice) {
    rl.question("Insert your choice : ",choice=>{
    if (choice==1) {
        cube();
    } else if (choice==2) {
        cone();
    } else if (choice==3) {
        process.exit();
    } else {
        console.log("Only input 1, 2 or 3");
        option();
    }
})
}
function testAgain (wish) {
    rl.question("Wanna test another score?[y/n]", wish=>{
        if (wish=="y") {
            option();
        } else if (wish=="n") {
            process.exit();
        } else {
            console.log("only input y or n");
            testAgain();
        }
    })
}
console.log("Choose what volume to calculate : ");
console.log("1. Cube");
console.log("2. Cone");
console.log("3. Exit");
option();
    
