const fs=require('fs')

const readFile=options=>file=>new Promise((resolve,reject)=>{
  fs.readFile(file,options,(err, content)=>{
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile=(file,content)=>new Promise((resolve,reject)=>{
  fs.writeFile(file,content,err=>{
    if (err) return reject(err)
    return resolve(content)
  })
})

const read=readFile('utf-8');
let result=''
/*Promise all*/
async function mergedContent() {
  try {
    const result=await Promise.all([
      read('contentPromise/content1.txt'),
      read('contentPromise/content2.txt'),
      read('contentPromise/content3.txt'),
      read('contentPromise/content4.txt'),
      read('contentPromise/content5.txt'),
      read('contentPromise/content6.txt'),
      read('contentPromise/content7.txt'),
      read('contentPromise/content8.txt'),
      read('contentPromise/content9.txt'),
      read('contentPromise/content10.txt')
    ])
    await writeFile('contentPromise/result.txt',result.join(""));
  } catch (e) {
    throw e
  }
  return read('contentPromise/result.txt');
}

mergedContent()
  .then(result=>{
    console.log("Ini hasil promise all : ",result);
  }).catch(err=>{
    console.log("Error to read/write file, error : ",err);
  }).finally(()=>{
    console.log("End of promise all");
  })

/*Promise chaining*/
read('contentPromise/content1.txt')
  .then(content1=>{
    result+=content1
    return read('contentPromise/content2.txt')
  }).then(content2=>{
    result+=content2
    return read('contentPromise/content3.txt')
  }).then(content3=>{
    result+=content3
    return read('contentPromise/content4.txt')
  }).then(content4=>{
    result+=content4
    return read('contentPromise/content5.txt')
  }).then(content5=>{
    result+=content5
    return read('contentPromise/content6.txt')
  }).then(content6=>{
    result+=content6
    return read('contentPromise/content7.txt')
  }).then(content7=>{
    result+=content7
    return read('contentPromise/content8.txt')
  }).then(content8=>{
    result+=content8
    return read('contentPromise/content9.txt')
  }).then(content9=>{
    result+=content9
    return read('contentPromise/content10.txt')
  }).then(content10=>{
    result+=content10
    return writeFile('contentPromise/result.txt',result)
  }).then(()=>{
    console.log("Writing Done!");
    return read('contentPromise/result.txt')
  }).then(result=>{
    console.log("Ini hasil promise chaining : ",result);
  }).catch(err=>{
    console.log('Error read/write file, error : ',err);
  })

  /* Promise Race */
  Promise.race([
    read('contentPromise/content1.txt'),read('contentPromise/content2.txt'),
    read('contentPromise/content3.txt'),read('contentPromise/content4.txt'),
    read('contentPromise/content5.txt'),read('contentPromise/content6.txt'),
    read('contentPromise/content7.txt'),read('contentPromise/content8.txt'),
    read('contentPromise/content9.txt'),read('contentPromise/content10.txt')
  ]).then(value=>{
    console.log("Ini hasil promice race : ",value);
  })
  .catch(error=>{
    console.log(error);
  })
