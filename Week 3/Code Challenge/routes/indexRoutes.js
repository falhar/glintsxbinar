const express=require('express')
const router=express.Router()
const indexController=require('../controllers/indexController.js')

router.get('/',indexController.home)
router.get('/firman',indexController.firman)

module.exports=router
