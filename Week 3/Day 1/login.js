const EventEmitter = require('events'); // Import
const readline = require('readline');
const status = require("./covidList.js");

 // Initialize an instance because it is a class
const my = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
let i="*";
rl._writeToOutput=function _writeToOutput(sandinya) {
  if (rl.stdoutMuted) {
    i+="*"
    rl.input.write("\x1b[2K\x1b[200D"+rl.query+i.slice(2,rl.line.length+2));
  } else {
    rl.output.write(sandinya);
  }
};
// Registering a listener
my.on("Login Failed", function(email) {
  // TODO: Saving the login trial count in the database
  console.log(`\n${email} is failed to login!`);
  rl.close();
})
my.on("Login Sukses", function(email) {
    status();
})
const user = {
    login(email, password) {
      const passwordStoredInDatabase = "123456";
  
      if (password !== passwordStoredInDatabase) {
        my.emit("Login Failed", email); // Pass the email to the listener
      } else {
        // Do something
        my.emit("Login Sukses", email);
      }
    }
  }
  
  rl.question("Email : ", function(email) {
    rl.query="Password : ";
    rl.stdoutMuted=true;
    rl.question(`Password : `,function(password) {
      rl.stdoutMuted=false;
      user.login(email, password) // Run login function
    })
  })
  module.exports.rl = rl
  