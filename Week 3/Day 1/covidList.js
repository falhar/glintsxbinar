// const readline=require("readline");
// const rl.rl=readline.createInterface({
//     input:process.stdin,
//     output:process.stdout
// });
const rl=require("./login.js");

let covid=[{
    name:"Anton",
    status:"Negative"
},
{
    name:"Bruno",
    status:"Suspect"
},
{
    name:"Narto",
    status:"Negative"
},
{
    name:"Catherine",
    status:"Suspect"
},
{
    name:"Hitler",
    status:"Positive"
},
{
    name:"Saskeh",
    status:"Negative"
},
{
    name:"Mismis",
    status:"Suspect"
},
{
    name:"Rias",
    status:"Positive"
},
{
    name:"Sakera",
    status:"Positive"
}]

function testAgain () {
    rl.rl.question("Wanna know more?[y/n]", wish=>{
        if (wish=="y") {
            console.clear();
            opsi();
        } else if (wish=="n") {
            console.clear();
            process.exit();
        } else {
            console.log("only input y or n");
            testAgain();
        }
    })
}
function positif() {
    let num=1;
    console.log("Positive Covid List : ");
    for (i=0;i<covid.length;i++) {
        if (covid[i].status=="Positive") {
            console.log(`${num}. ${covid[i].name}`);
            num+=1;
        }
    }
}
function negatif() {
    let num=1;
    console.log("Negative Covid List : ");
    for (i=0;i<covid.length;i++) {
        if (covid[i].status=="Negative") {
            console.log(`${num}. ${covid[i].name}`);
            num+=1;
        }
    }
}
function suspect() {
    let num=1;
    console.log("Suspect Covid List : ");
    for (i=0;i<covid.length;i++) {
        if (covid[i].status=="Suspect") {
            console.log(`${num}. ${covid[i].name}`);
            num+=1;
        }
    }
}
function opsi() {
    console.clear();
    console.log("Status Covid");
    console.log("============");
    console.log("1. Positive");
    console.log("2. Negative");
    console.log("3. Suspect");
    console.log("4. Exit");
    rl.rl.question("Insert choice : ", pil=>{
        console.clear();
        switch(Number(pil)) {
            case 1:
                positif();
                testAgain();
                break;
            case 2:
                negatif();
                testAgain();
                break;
            case 3:
                suspect();
                testAgain();
                break;
            case 4:
                rl.rl.close();
                break;
            default:
                console.log("Invalid choice");
                testAgain();
        }
    })
}


// opsi();
module.exports=opsi